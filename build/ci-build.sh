#!/bin/bash
SERVER_IP=$1
CI_COMMIT_REF_NAME=$2
ABSOLUTEPATH=$3
VER_CHANNEL=$4
VER_MAJOR=$5
VER_MILESTONE=$6
VER_WEEKLY=$7
VER_REVISED=$8

branchId=1
if [[ `echo $CI_COMMIT_REF_NAME | grep "alpha"` != "" ]]; then
  branchId=1
elif [[ `echo $CI_COMMIT_REF_NAME | grep "beta"` != "" ]]; then
  branchId=2
elif [[ `echo $CI_COMMIT_REF_NAME | grep "release"` != "" ]]; then
  branchId=3
else
  branchId=4
fi
TARGERPATH=$ABSOLUTEPATH/$VER_CHANNEL/${VER_MAJOR}.${VER_MILESTONE}.${VER_WEEKLY}.${VER_REVISED}
export NODE_PORT=$((30000 + ($VER_MAJOR + 50)*100 + $VER_MILESTONE*100 + $VER_WEEKLY + $branchId + 20 + $VER_CHANNEL + 10))
echo "start build"
export NODE_HOST=$SERVER_IP
export NODE_BRANCH=$CI_COMMIT_REF_NAME
# export NODE_PORT=$((30000 + ($VER_MAJOR + 50)*100 + $VER_MILESTONE*100 + $VER_WEEKLY + $branchId + 20 + $VER_CHANNEL + 10))
if [[ `echo $CI_COMMIT_REF_NAME | grep "release"` != "" ]]; then
  export NODE_ENV=production
elif [[ `echo $CI_COMMIT_REF_NAME | grep "ga"` != "" ]]; then
  export NODE_ENV=production
else
  export NODE_ENV=development
fi
yarn run quasar build -m spa

if [ ! -d "dist" ]; then
  mkdir dist
fi
cd dist/spa
tar czf admin.tar.gz * 
expect << _EOF
set timeout -1
spawn ssh -p 22 root@118.190.206.94
expect {
  "*yes/no)?" {send "yes\n"; exp_continue}
  "*assword:" {send "pAssW0rd!\n"}
}
expect "*]#" {send "mkdir -p $TARGERPATH\n"}
expect "*]#" {send "logout\n"}
expect eof

spawn scp admin.tar.gz root@118.190.206.94:$TARGERPATH
expect "*assword:" {send "pAssW0rd!\n"}
expect eof

spawn ssh -p 22 root@118.190.206.94
expect {
  "*yes/no)?" {send "yes\n"; exp_continue}
  "*assword:" {send "pAssW0rd!\n"}
}
expect "*]#" {send "cd $TARGERPATH\n"}
expect "*]#" {send "tar -zxvf admin.tar.gz\n"}
expect "*]#" {send "logout\n"}
expect eof

_EOF