#!/bin/bash
INDEXURL=$1
SERVERIP=$2
QAULITYPATH=$3

yarn run audit:lighthouse:CI  $INDEXURL view --output-path=./test/lighthouse/reports/index.html --config-path=./test/lighthouse/configs/full-config.js
cd test/lighthouse
tar czf lighthouse.tar.gz reports/*
expect << __EOF
spawn ssh -p 22 root@${SERVERIP}
expect {
  "*yes/no)?" {
    send "yes\n";
    exp_continue
  }
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "mkdir -p ${QAULITYPATH}\n"}
expect "*]#" {send "logout\n"}
expect eof

spawn scp lighthouse.tar.gz root@${SERVERIP}:${QAULITYPATH}
expect "*assword:" {send "pAssW0rd!\n"}
expect eof

spawn ssh -p 22 root@${SERVERIP}
expect {
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "cd $QAULITYPATH\n"}
expect "*]#" {send "tar -zxvf lighthouse.tar.gz\n"}
expect "*]#" {send "rm -rf lighthouse.tar.gz\n"}
expect "*]#" {send "logout\n"}
expect eof

__EOF