export default function (request) {
  return {
    login: function (username, password) {
      return request({
        url: '/v1/admin/login',
        method: 'POST',
        data: {
          admin_name: username,
          admin_pwd: password
        }
      })
    },
    logout: function () {
      return request({
        url: '/v1/admin/logout',
        needToken: true,
        method: 'POST'
      })
    },
    register: function (username, password) {
      return request({
        url: '/v1/admin/register',
        method: 'POST',
        data: {
          admin_name: username,
          admin_pwd: password
        }
      })
    },
    resetPassword: function (username, oldPassword, newPassword) {
      return request({
        url: '/v1/admin/resetPwd',
        needToken: true,
        method: 'POST',
        data: {
          admin_name: username,
          admin_pwd: oldPassword,
          admin_pwd_new: newPassword
        }
      })
    },
    getAllCount: function () {
      return request({
        url: '/v1/admin/getAllCount',
        needToken: true,
        method: 'GET'
      })
    },
    getAllList: function ({ limit, offset }) {
      return request({
        url: '/v1/admin/getAllList',
        needToken: true,
        method: 'POST',
        data: { limit, offset }
      })
    }
  }
}
