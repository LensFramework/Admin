export default function (request) {
  return {
    getAllCount: function ({ status }) {
      return request({
        url: '/v1/article/getAllCount',
        needToken: true,
        method: 'POST',
        data: {
          status
        }
      })
    },
    getAllList: function ({ limit, offset, status }) {
      return request({
        url: '/v1/article/getAllList',
        needToken: true,
        method: 'POST',
        data: { limit, offset, status }
      })
    },
    display: function ({ articleId }) {
      return request({
        url: '/v1/article/display',
        needToken: true,
        method: 'POST',
        data: { article_id: articleId }
      })
    }
  }
}
