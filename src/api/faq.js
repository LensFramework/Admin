export default function (request) {
  return {
    getAllCount () {
      return request({
        url: '/v1/faq/getAllCount',
        method: 'GET',
        needToken: true
      })
    },
    getAllFaq ({ limit, offset }) {
      return request({
        url: '/v1/faq/getAllFaq',
        method: 'POST',
        needToken: true,
        data: {
          limit, offset
        }
      })
    },
    addFaq ({ title, content }) {
      return request({
        url: '/v1/faq/addFaq',
        method: 'POST',
        needToken: true,
        data: {
          title, content
        }
      })
    },
    removeFaq ({ id }) {
      return request({
        url: '/v1/faq/removeFaq',
        method: 'POST',
        needToken: true,
        data: {
          f_id: id
        }
      })
    },
    editFaq ({ id, title, content }) {
      return request({
        url: '/v1/faq/removeFaq',
        method: 'POST',
        needToken: true,
        data: {
          f_id: id,
          title,
          content
        }
      })
    }
  }
}
