export default function (request) {
  return {
    getAllNotProcessed ({ limit, offset }) {
      return request({
        url: '/v1/feedback/getAllNotProcessed',
        method: 'POST',
        needToken: true,
        data: {
          limit, offset
        }
      })
    },
    getAllAlreadyProcessed ({ limit, offset }) {
      return request({
        url: '/v1/feedback/getAllAlreadyProcessed',
        method: 'POST',
        needToken: true,
        data: {
          limit, offset
        }
      })
    },
    getAllCountByIsReplied ({ isReplied }) {
      return request({
        url: '/v1/feedback/getAllCountByIsReplied',
        method: 'POST',
        needToken: true,
        data: {
          isReplied
        }
      })
    }
  }
}
