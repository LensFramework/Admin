const files = require.context('.', true, /\.js$/)
const Path = require('path')
export default function (request) {
  let config = {}
  files.keys().forEach(key => {
    if (key === './index.js') return
    config[Path.basename(key, '.js')] = files(key).default(request)
  })
  return config
}
