export default function (request) {
  return {
    getUsersList: function ({ limit, offset, userLock, userFreeze }) {
      return request({
        url: '/v1/user/getUsersList',
        method: 'POST',
        needToken: true,
        data: {
          limit,
          offset,
          user_lock: userLock,
          user_freeze: userFreeze
        }
      })
    },
    getUsersCount: function ({ userLock, userFreeze }) {
      return request({
        url: '/v1/user/getUsersCount',
        method: 'POST',
        needToken: true,
        data: {
          user_lock: userLock,
          user_freeze: userFreeze
        }
      })
    },
    getUsersDetailInfo ({ userId }) {
      return request({
        url: '/v1/user/getUsersDetailInfo',
        method: 'POST',
        needToken: true,
        data: {
          user_id: userId
        }
      })
    }
  }
}
