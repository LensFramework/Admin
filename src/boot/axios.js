import axios from 'axios'
import { Cookies, Notify } from 'quasar'
import api from '../api'
import config from '../config'
import statusCode from '../utils/statusCode'
export default ({ app, router, Vue }) => {
  const service = axios.create({
    baseURL: process.env.BASE_API,
    timeout: 15000
  })
  service.interceptors.request.use(req => {
    if (req.needToken) {
      req.headers['X-Token'] = Cookies.get(config.cookies.login)
    }
    req.headers['X-Role'] = Cookies.get(config.cookies.role) // app.store.getters['app/role']
    req.headers['Content-Type'] = 'application/json'
    console.log('request', req)
    return req
  }, error => {
    console.log(error)
    Promise.reject(error)
  })
  service.interceptors.response.use(response => {
    const res = response.data
    console.log('response', response)
    if (res.code !== statusCode.success) {
      if (res.code === statusCode.unknown) {
        Notify.create('未知错误')
      } else if (res.code === statusCode.otherLogin) {
        Notify.create('你已被登出')
        app.store.dispatch('login/Logout')
      } else if (res.code === statusCode.tokenInvalid || res.code === statusCode.roleInvalid) {
        console.log('woso')
        app.store.dispatch('login/Logout')
        app.store.dispatch('route/push', { path: '/login/', name: 'login' })
      } else {
        Notify.create(res.code + ':' + res.message)
      }
      return Promise.reject(res.message)
    } else {
      // app.store.dispatch('socket/socketLogin')
      return response.data.data
    }
  }, error => {
    console.error(error)
    Notify.create('请求错误')
    return Promise.reject(error)
  })
  Vue.prototype.$api = api(service)
}
