import vuePicturePreview from 'vue-picture-preview'
import Navigation from 'vue-navigation'
import VeLine from 'v-charts/lib/line.common'
// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.use(Navigation, {
    router,
    store: app.store,
    moduleName: 'navigation',
    keyName: 'VNK'
  })
  Vue.use(vuePicturePreview)
  Vue.component(VeLine.name, VeLine)
}
