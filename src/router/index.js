import Vue from 'vue'
import VueRouter from 'vue-router'
import moduleRouters from './modules'
import commonRouters from './common'
function sync (store, router, options) {
  const moduleName = (options || {}).moduleName || 'route'
  const configModuleName = (options || {}).configModuleName || 'routeConfig'
  const moduleConfig = (options || {}).moduleConfig
  store.registerModule(moduleName, {
    namespaced: true,
    state: cloneRoute(router.currentRoute),
    mutations: {
      'ROUTE_CHANGED' (state, transition) {
        store.state[moduleName] = cloneRoute(transition.to, transition.from)
      }
    },
    actions: {
      push ({ commit }, route) {
        console.log(route)
        router.push(route)
      }
    }
  })
  store.registerModule(configModuleName, {
    namespaced: true,
    state: moduleConfig
  })

  let isTimeTraveling = false
  let currentPath

  // sync router on store change
  const storeUnwatch = store.watch(
    state => state[moduleName],
    route => {
      const { fullPath } = route
      if (fullPath === currentPath) {
        return
      }
      if (currentPath != null) {
        isTimeTraveling = true
        router.push(route)
      }
      currentPath = fullPath
    },
    { sync: true }
  )

  // sync store on router navigation
  const afterEachUnHook = router.afterEach((to, from) => {
    if (isTimeTraveling) {
      isTimeTraveling = false
      return
    }
    currentPath = to.fullPath
    store.commit(moduleName + '/ROUTE_CHANGED', { to, from })
  })

  return function unsync () {
    // On unsync, remove router hook
    if (afterEachUnHook != null) {
      afterEachUnHook()
    }

    // On unsync, remove store watch
    if (storeUnwatch != null) {
      storeUnwatch()
    }

    // On unsync, unregister Module with store
    store.unregisterModule(moduleName)
  }
}
function cloneRoute (to, from) {
  const clone = {
    name: to.name,
    path: to.path,
    hash: to.hash,
    query: to.query,
    params: to.params,
    fullPath: to.fullPath,
    meta: to.meta
  }
  if (from) {
    clone.from = cloneRoute(from)
  }
  return Object.freeze(clone)
}
Vue.use(VueRouter)
function getChildRouteConfig (ele, parentPath) {
  let config = {}
  if (parentPath) {
    config.path = parentPath + '/' + ele.path
    parentPath = parentPath + '/' + ele.path
  } else {
    config.path = ele.path
    parentPath = ele.path
  }
  config.name = ele.name
  config.meta = ele.meta
  if (ele.children) {
    config.children = {}
    ele.children.forEach(e => {
      config.children[e.name] = getChildRouteConfig(e, parentPath)
    })
  } else {
    if (ele.path === undefined || ele.name === undefined) {
      console.error('router must has path and name:' + ele)
    }
  }
  return config
}
export function getRouteConfig () {
  let routeConfig = {}
  let routes = []
  for (let mod in moduleRouters) {
    let config = {}
    moduleRouters[mod].forEach(ele => {
      config[ele.name] = getChildRouteConfig(ele)
    })
    routeConfig[mod] = config
    routes = routes.concat(moduleRouters[mod])
  }
  return { routeConfig, routes }
}
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
export default function ({ store, ssrContext }) {
  let conf = getRouteConfig()
  let routes = conf.routes
  let routeConfig = conf.routeConfig
  routes = routes.concat(commonRouters)
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes: routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.routeConfig = routeConfig
  console.log('routeConfig', routeConfig)
  sync(store, Router, {
    moduleName: 'route',
    configModuleName: 'routeConfig',
    moduleConfig: routeConfig
  })
  return Router
}
