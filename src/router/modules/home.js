export default [
  {
    path: '/home',
    name: 'home',
    redirect: '/home/index',
    component: () => import('layouts/generalLayout.vue'),
    meta: {
      title: '',
      icon: 'home'
    },
    children: [
      {
        path: 'index',
        name: 'index',
        meta: {
          title: '控制中心',
          icon: 'inbox',
          id: 1
        },
        components: {
          default: () => import('pages/index.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        }
      },
      {
        path: 'users',
        name: 'users',
        components: {
          default: () => import('pages/users/index.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        },
        meta: {
          title: '用户中心',
          icon: 'inbox',
          id: 1
        },
        children: [
          {
            path: 'userList',
            name: 'userList',
            component: () => import('pages/users/userList/index.vue'),
            meta: {
              title: '用户列表',
              icon: 'inbox',
              id: 1
            },
            children: [
              {
                path: 'actived',
                name: 'actived',
                component: () => import('pages/users/userList/actived.vue'),
                meta: {
                  title: '活跃用户',
                  icon: 'inbox',
                  id: 1,
                  canRoute: true
                }
              },
              {
                path: 'freezed',
                name: 'freezed',
                component: () => import('pages/users/userList/freezed.vue'),
                meta: {
                  title: '冻结用户',
                  icon: 'inbox',
                  id: 1,
                  canRoute: true
                }
              },
              {
                path: 'locked',
                name: 'locked',
                component: () => import('pages/users/userList/locked.vue'),
                meta: {
                  title: '锁定用户',
                  icon: 'inbox',
                  id: 1,
                  canRoute: true
                }
              }
            ]
          },
          {
            path: 'statistics',
            name: 'statistics',
            component: () => import('pages/users/statistics.vue'),
            meta: {
              title: '数据统计',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          },
          {
            path: 'feedback',
            name: 'feedback',
            component: () => import('pages/users/feedback/index.vue'),
            meta: {
              title: '用户反馈',
              icon: 'inbox',
              id: 1
            },
            children: [
              {
                path: 'notProcessed',
                name: 'notProcessed',
                components: {
                  default: () => import('pages/users/feedback/notProcessed.vue')
                },
                meta: {
                  title: '未处理的反馈',
                  icon: 'inbox',
                  id: 1,
                  canRoute: true
                }
              },
              {
                path: 'alreadyProcessed',
                name: 'alreadyProcessed',
                components: {
                  default: () => import('pages/users/feedback/alreadyProcessed.vue')
                },
                meta: {
                  title: '已处理的反馈',
                  icon: 'inbox',
                  id: 1,
                  canRoute: true
                }
              }
            ]
          },
          {
            path: 'faq',
            name: 'faq',
            components: {
              default: () => import('pages/users/faq.vue')
            },
            meta: {
              title: '常见FAQ设置',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          }
        ]
      },
      {
        path: 'contents',
        name: 'contents',
        components: {
          default: () => import('pages/contents/index.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        },
        meta: {
          title: '内容管理',
          icon: 'inbox',
          id: 1
        },
        children: [
          {
            path: 'audited',
            name: 'audited',
            component: () => import('pages/contents/audited.vue'),
            meta: {
              title: '已审核',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          },
          {
            path: 'review',
            name: 'review',
            component: () => import('pages/contents/review.vue'),
            meta: {
              title: '待审核',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          },
          {
            path: 'removed',
            name: 'removed',
            component: () => import('pages/contents/removed.vue'),
            meta: {
              title: '已下架',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          }
        ]
      },
      {
        path: 'application',
        name: 'application',
        components: {
          default: () => import('pages/application/index.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        },
        meta: {
          title: '应用管理',
          icon: 'inbox',
          id: 1
        },
        children: [
          {
            path: 'ab',
            name: 'ab',
            component: () => import('pages/application/ab.vue'),
            meta: {
              title: '广告配置',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          }
        ]
      },
      {
        path: 'messagePush',
        name: 'messagePush',
        components: {
          default: () => import('pages/messagePush.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        },
        meta: {
          title: '消息推送',
          icon: 'inbox',
          id: 1,
          canRoute: true
        }
      },
      {
        path: 'settings',
        name: 'settings',
        components: {
          default: () => import('pages/settings/index.vue'),
          nav: () => import('pages/header.vue'),
          leftDrawer: () => import('pages/menu.vue')
        },
        meta: {
          title: '系统设置',
          icon: 'inbox',
          id: 1
        },
        children: [
          {
            path: 'administrators',
            name: 'administrators',
            component: () => import('pages/settings/administrators.vue'),
            meta: {
              title: '管理员设置',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          },
          {
            path: 'otherInterface',
            name: 'otherInterface',
            component: () => import('pages/settings/otherInterface.vue'),
            meta: {
              title: '第三方接口设置',
              icon: 'inbox',
              id: 1,
              canRoute: true
            }
          }
        ]
      }
    ]
  }
]
