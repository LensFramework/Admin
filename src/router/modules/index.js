const files = require.context('.', true, /\.js$/)
const Path = require('path')
let config = {}
files.keys().forEach(key => {
  if (key === './index.js') return
  config[Path.basename(key, '.js')] = files(key).default
})
export default config
