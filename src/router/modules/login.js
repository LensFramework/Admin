export default [
  {
    path: '/login',
    name: 'login',
    component: () => import('layouts/emptyLayout.vue'),
    children: [
      {
        path: '',
        name: 'index',
        component: () => import('pages/login.vue')
      }
    ]
  }
]
