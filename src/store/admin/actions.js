import Vue from 'vue'
export function getAllCount (context) {
  return Vue.prototype.$api.admin.getAllCount()
}
export function getAllList (context, data) {
  return Vue.prototype.$api.admin.getAllList(data)
}
