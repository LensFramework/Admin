
export function openLeftDrawer (state) {
  return state.openLeftDrawer
}
export function role (state) {
  return state.role
}
export function visitedViews (state) {
  return state.visitedViews
}
