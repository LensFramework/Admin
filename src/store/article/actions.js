import Vue from 'vue'
export function getAllCount (context, data) {
  return Vue.prototype.$api.article.getAllCount(data)
}
export function getAllList (context, data) {
  return Vue.prototype.$api.article.getAllList(data)
}
export function display (context, data) {
  return Vue.prototype.$api.article.display(data)
}
