import Vue from 'vue'
export function getAllCountByIsReplied (context, data) {
  return Vue.prototype.$api.feedback.getAllCountByIsReplied(data)
}
export function getAllNotProcessed (context, data) {
  return Vue.prototype.$api.feedback.getAllNotProcessed(data)
}
