import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { SessionStorage } from 'quasar'
import login from './login'
import app from './app'
import users from './users'
import feedback from './feedback'
import faq from './faq'
import article from './article'
import admin from './admin'
Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */
export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      login,
      app,
      users,
      feedback,
      faq,
      article,
      admin
    },
    plugins: [
      createPersistedState({
        storage: {
          getItem: key => SessionStorage.getItem(key),
          setItem: (key, value) => SessionStorage.set(key, value, { expires: 3, secure: true }),
          removeItem: key => SessionStorage.remove(key)
        }
      })
    ],
    strict: process.env.NODE_ENV !== 'production'
  })
  return Store
}
