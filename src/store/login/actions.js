import { Cookies } from 'quasar'
import Vue from 'vue'
import Config from '../../../src/config'
export function Login ({ commit, dispatch }, { username, password }) {
  return new Promise((resolve, reject) => {
    Vue.prototype.$api.admin.login(username, password).then(data => {
      commit('app/SET_TOKEN', data.token, { root: true })
      commit('app/SET_ROLE', 'admin', { root: true })
      Cookies.set(Config.cookies.login, data.token)
      Cookies.set(Config.cookies.role, 'admin')
      resolve()
    }).catch((err) => {
      reject(err)
    })
  })
}
export function Logout ({ commit, dispatch, state, rootState }) {
  return new Promise(resolve => {
    console.log(rootState)
    commit('app/SET_TOKEN', null, { root: true })
    commit('app/SET_ROLE', '', { root: true })
    Cookies.set(Config.cookies.login, null)
    Cookies.set(Config.cookies.role, '')
    resolve()
  })
}
