import Vue from 'vue'
export function getUsersList ({ commit }, data) {
  return Vue.prototype.$api.user.getUsersList(data)
}
export function getUsersCount ({ commit }, data) {
  return Vue.prototype.$api.user.getUsersCount(data)
}
export function getUsersDetailInfo ({ commit }, data) {
  return Vue.prototype.$api.user.getUsersDetailInfo(data)
}
