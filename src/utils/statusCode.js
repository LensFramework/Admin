export default {
  success: 20000, // 成功
  otherLogin: 50012, // 其他客户端登录了
  tokenInvalid: 50014, // Token 过期了
  roleInvalid: 60001, // 非法的token
  unknown: 50000 // 非法的token
}
